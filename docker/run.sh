#!/bin/sh

set -e -x

if [ -z "$NO_MIGRATIONS" ]
then
    python manage.py migrate --no-input
fi

if [ -n "$INIT_MINIO_BUCKET" ]
then
    python manage.py createbucket
fi

chown $APP_USER $APP_ROOT/media

exec su \
     --preserve-environment \
     --command "uvicorn --host 0.0.0.0 $@ opentour.asgi:application" \
     "$APP_USER"
