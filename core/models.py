from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from polymorphic.models import PolymorphicModel
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.exceptions import ValidationError
from colorfield.fields import ColorField
from djgeojson.fields import PolygonField

import uuid
from furl import furl

from core.utils import qr_from_url


class Questline(models.Model):
    """
    Questline model
    """
    title = models.CharField(max_length=255)
    description = RichTextUploadingField()
    image = models.ImageField(blank=True)

    def __str__(self):
        return self.title


class Quest(PolymorphicModel):
    """
    Quest model
    """
    name = models.CharField(max_length=255)
    description = RichTextUploadingField()
    questline = models.ForeignKey(Questline, default=None, blank=True,
                                  on_delete=models.CASCADE,
                                  related_name="quests")
    experience = models.IntegerField(default=0)
    image = models.ImageField(blank=True)
    order = models.PositiveIntegerField(default=0)
    show_next = models.BooleanField(default=False)

    @property
    def previous_quest(self):
        return Quest.objects.filter(order=self.order - 1,
                                    questline=self.questline).first()

    @property
    def next_quest(self):
        return Quest.objects.filter(order=self.order + 1,
                                    questline=self.questline).first()

    @property
    def next_quest_id(self):
        next_quest = self.next_quest

        if next_quest:
            return next_quest.id

        return None

    @property
    def next_quest_type(self):
        next_quest = self.next_quest

        if next_quest:
            return next_quest._meta.object_name

        return None

    def __str__(self):
        return self.name

    def generate_url(self, root):
        url = furl(root)

        if settings.FRONTEND_URL:
            url = furl(settings.FRONTEND_URL)

        return url.join("quest/").join(self.id).add(
            {'type': self._meta.object_name})

    def generate_qr(self, root):
        """Create a QR code PIL image."""

        url = self.generate_url(root)

        return qr_from_url(url)

    def clean(self):
        if self.order == 0:
            raise ValidationError("The first quest must be an unprotected"
                                  " quest.")

    class Meta:
        ordering = ["order"]


class UnprotectedQuest(Quest):
    """
    Unprotected quest without verification method.
    """

    def clean(self):
        # Override first quest check of base model
        pass


class GeofencingQuest(Quest):
    """
    Quest model with geofencing support
    """
    polygon = PolygonField()


class PasswordQuest(Quest):
    """
    Quest model with password support
    """
    password = models.CharField(max_length=16)
    embed_password = models.BooleanField(default=True)

    def generate_url(self, root):
        url = super().generate_url(root)

        if self.embed_password:
            url.add({'password': self.password})

        return url


class BroadcastMessage(models.Model):
    """
    BroadcastMessage model
    """
    short_description = models.CharField(max_length=255)
    full_description = RichTextUploadingField()
    color = ColorField(default="#E34140")
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return self.short_description


class Team(models.Model):
    name = models.CharField(max_length=255)
    invite_code = models.CharField(max_length=255, blank=True)

    @property
    def experience(self):
        return sum([user.experience for user in self.users.all()])

    @property
    def level(self):
        return self.experience // 1000

    def generate_qr(self, root):
        """Create a QR code PIL image."""

        url = furl(root)
        if settings.FRONTEND_URL:
            url = furl(settings.FRONTEND_URL)

        url.join("team/join/")
        url.args["code"] = self.invite_code

        return qr_from_url(url)

    def __str__(self):
        return self.name


@receiver(post_save, sender=Team)
def create_profile(sender, instance, created, **kwargs):
    """Generate an invitation code."""
    if created:
        instance.invite_code = uuid.uuid4().hex
        instance.save()


class User(AbstractUser):
    team = models.ForeignKey(Team, default=None, blank=True,
                             on_delete=models.SET_NULL,
                             related_name="users", null=True)
    finished_quests = models.ManyToManyField(Quest)

    @property
    def experience(self):
        return sum([x.experience for x in self.finished_quests.all()])

    @property
    def level(self):
        return self.experience // 1000

    @property
    def finished_questlines(self):
        ids = (self.finished_quests
                   .order_by()
                   .values('questline')
                   .distinct())

        return Questline.objects.filter(id__in=ids)
