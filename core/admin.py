from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.forms import BaseInlineFormSet

from nested_admin import (
    NestedPolymorphicModelAdmin,
    SortableHiddenMixin,
    NestedStackedPolymorphicInline
)

from .models import (
    Quest,
    PasswordQuest,
    UnprotectedQuest,
    GeofencingQuest
)
from .models import Questline
from .models import BroadcastMessage
from .models import User

from leaflet.forms.widgets import LeafletWidget
from leaflet.admin import LeafletGeoAdminMixin


class QuestForm(forms.ModelForm):

    class Meta:
        model = Quest
        fields = '__all__'

    def clean_password(self):
        return self.cleaned_data['password']


class QuestInlineFormSet(BaseInlineFormSet):
    pass


class QuestInline(SortableHiddenMixin, NestedStackedPolymorphicInline):
    """
    Inline-stacked Quest fields under Questline admin interface.
    """

    class UnprotectedQuestInline(NestedStackedPolymorphicInline.Child):
        model = UnprotectedQuest

    class PasswordQuestInline(NestedStackedPolymorphicInline.Child):
        model = PasswordQuest

    class GeofencingQuestInline(LeafletGeoAdminMixin,
                                NestedStackedPolymorphicInline.Child):
        model = GeofencingQuest
        widgets = {"polygon": LeafletWidget()}

    model = Quest
    extra = 0
    sortable_field_name = "order"
    child_inlines = (
        UnprotectedQuestInline,
        PasswordQuestInline,
        GeofencingQuestInline
    )


class QuestlineAdmin(NestedPolymorphicModelAdmin):
    """
    Questline admin interface with nested and
    orderable Quests.
    """

    inlines = [
        QuestInline,
    ]

    change_form_template = "admin/questline/change_list.html"


class BroadcastMessageAdmin(admin.ModelAdmin):
    model = BroadcastMessage


UserAdmin.fieldsets += (
    (
        'Game profile',
        {
            'fields': ('finished_quests', 'team')
        }
    ),
)

admin.site.site_title = "OpenTour"
admin.site.site_header = "OpenTour Administration"
admin.site.index_title = "Welcome to OpenTour administration panel"

admin.site.register(Questline, QuestlineAdmin)
admin.site.register(BroadcastMessage, BroadcastMessageAdmin)
admin.site.register(User, UserAdmin)
