from rest_framework.routers import DefaultRouter
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, \
    TokenRefreshView

from core import views

app_name = "core"

router = DefaultRouter()
router.register("quest", views.QuestViewSet)
router.register("questline", views.QuestlineViewSet)
router.register("preferences", views.PreferencesViewSet)
router.register("broadcast", views.BroadcastMessageViewSet)
router.register("team", views.TeamViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("sync_quests/", views.QuestSyncView.as_view(), name='sync_quests'),
    path("token/", TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path("token/refresh/", TokenRefreshView.as_view(), name='token_refresh'),
    path('registration/', include('rest_registration.api.urls'))
]
