from rest_framework.permissions import BasePermission
from rest_framework.serializers import ValidationError
from core.models import (
    PasswordQuest,
    GeofencingQuest
)
from shapely.geometry import (
    asPolygon,
    Point
)


class QuestPermission(BasePermission):
    """
    Permissions for QuestViewSet.
    """

    def check_password(self, request, obj):
        if not obj.password:
            return True

        return ("password" in request.query_params and
                request.query_params["password"] == obj.password)

    def is_finished(self, request, obj):
        if not hasattr(request.user, "finished_quests"):
            return False

        if obj in request.user.finished_quests.all():
            return True

    def check_location(self, request, obj):
        if "location" not in request.query_params:
            return False

        # ensure input data is clean
        try:
            location = request.query_params["location"].split(",")
            location_north = float(location[0])
            location_east = float(location[1])
        except IndexError:
            raise ValidationError(
                {"detail": "Incorrect format, must be 'north,east'."}
            )
        except ValueError:
            raise ValidationError(
                {"detail": "Coordinates do not contain string."}
            )

        # create objects and check location
        point = Point(location_east, location_north)

        # multi-polygon for future
        for polygon in obj.polygon["coordinates"]:
            if asPolygon(polygon).contains(point):
                return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff or request.user.is_superuser:
            return True

        if self.is_finished(request, obj):
            return True

        if isinstance(obj, PasswordQuest):
            return self.check_password(request, obj)

        if isinstance(obj, GeofencingQuest):
            return self.check_location(request, obj)

        else:
            return True


class TeamPermission(BasePermission):
    """
    Permissions for TeamViewSet.
    """

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff or request.user.is_superuser:
            return True
        if not request.user.is_authenticated:
            return False
        return request.user.team == obj
