from rest_framework.serializers import (
    ModelSerializer, PrimaryKeyRelatedField
)
from rest_polymorphic.serializers import PolymorphicSerializer

from dynamic_preferences.models import GlobalPreferenceModel

from .models import (Quest,
                     GeofencingQuest,
                     PasswordQuest,
                     Questline,
                     BroadcastMessage,
                     User,
                     Team)


class QuestlineSerializer(ModelSerializer):
    """
    Serializer for Questline model.
    """

    quests = PrimaryKeyRelatedField(
        many=True,
        read_only=True
    )

    class Meta:
        model = Questline
        fields = ("id", "title", "description", "quests", "image")


class QuestSerializer(ModelSerializer):
    """
    Serializer for Quest model.

    Works for all quest models as long as
    you don't need more fields.
    """
    class Meta:
        model = Quest
        fields = ("id", "name", "description",
                  "questline", "image", "order", "show_next",
                  "next_quest_id", "next_quest_type")


class QuestPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Quest: QuestSerializer,
        GeofencingQuest: QuestSerializer,
        PasswordQuest: QuestSerializer,
    }


class PreferencesSerializer(ModelSerializer):

    class Meta:
        model = GlobalPreferenceModel
        fields = ("name", "raw_value")


class BroadcastMessageSerializer(ModelSerializer):
    """
    Serializer for BroadcastMessage model.
    """
    class Meta:
        model = BroadcastMessage
        fields = ("id", "short_description", "full_description", "color",
                  "start", "end")


class UserSerializer(ModelSerializer):
    """
    Serializer for User model.
    """
    class Meta:
        model = User
        fields = ("username", "experience", "level", "finished_quests", "team")
        read_only_fields = ("experience", "level", "finished_quests", "team")


class TeamSerializer(ModelSerializer):
    """
    Serializer for Team Model
    """

    users = UserSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = Team
        fields = ("id", "name", "users", "invite_code", "experience", "level")
        read_only_fields = ("id", "users", "invite_code", "experience",
                            "level")


class QuestListSerializer(ModelSerializer):
    """
    Serializer for a list of quest ids.
    Raises a ValidationError if one of the quest ids is not in the database.
    """
    finished_quests = PrimaryKeyRelatedField(many=True,
                                             queryset=Quest.objects.all())

    class Meta:
        model = User
        fields = ("finished_quests",)


class QuestSyncSerializer(ModelSerializer):
    finished_quests = QuestSerializer(read_only=True,
                                      many=True)
    finished_questlines = QuestlineSerializer(read_only=True,
                                              many=True)

    class Meta:
        model = User
        fields = ("finished_quests", "finished_questlines")
