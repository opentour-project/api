from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from minio import Minio
from minio.error import S3Error

from urllib.parse import urlparse
import json


class Command(BaseCommand):
    help = 'Creates configured bucket for minio servers'

    _bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        "*",
                    ],
                },
                "Action": [
                    "s3:GetBucketLocation",
                    "s3:ListBucket",
                ],
                "Resource": [
                    "arn:aws:s3:::media",
                ],
            },
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        "*",
                    ],
                },
                "Action": [
                    "s3:GetObject",
                ],
                "Resource": [
                    "arn:aws:s3:::media/*",
                ],
            },
        ],
    }

    def handle(self, *args, **options):
        if not settings.AWS_S3_ENDPOINT_URL:
            raise CommandError('This is meant for minio servers')

        minio_host = urlparse(settings.AWS_S3_ENDPOINT_URL).netloc

        bucket_name = settings.AWS_STORAGE_BUCKET_NAME

        client = Minio(minio_host,
                       access_key=settings.AWS_ACCESS_KEY_ID,
                       secret_key=settings.AWS_SECRET_ACCESS_KEY,
                       secure=settings.AWS_S3_USE_SSL)

        try:
            client.make_bucket(bucket_name)
            client.set_bucket_policy(
                bucket_name,
                json.dumps(self._bucket_policy)
            )
        except S3Error:
            pass

        self.stdout.write(
            self.style.SUCCESS(
                f'Successfully created bucket "{bucket_name}".'
            )
        )
