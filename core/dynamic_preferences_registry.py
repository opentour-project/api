
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.serializers import StringSerializer
from dynamic_preferences.types import (
    BooleanPreference,
    StringPreference,
    BasePreferenceType
)

from django.forms import CharField
from colorfield.fields import ColorWidget


@global_preferences_registry.register
class Title(StringPreference):
    """
    Instance title.
    """
    name = "title"
    default = "OpenTour"
    help_text = """Name for this OpenTour instance.
        It is the website title that your users will see."""
    required = True


@global_preferences_registry.register
class Motd(StringPreference):
    """
    Instance motd.
    """
    name = "motd"
    default = "Welcome on this OpenTour instance!"
    help_text = """This the message of the day your users will see!
        Provide them a nice and warm welcome message!"""
    required = True


@global_preferences_registry.register
class ColorAccent(BasePreferenceType):
    """
    Instance color.
    """
    field_class = CharField
    serializer = StringSerializer
    widget = ColorWidget

    name = "color_accent"
    default = "#1074e7"
    help_text = """This is the color accent for the frontend application.
        It only accepts HTML formated colors."""
    required = True


@global_preferences_registry.register
class MaintenanceMode(BooleanPreference):
    """
    Instance maintenance mode.
    """
    name = "maintenance_mode"
    default = False
    help_text = "Enable maintenance mode for frontend and users"
    required = True
