from django.http import HttpResponse
from django.http import FileResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from dynamic_preferences.models import GlobalPreferenceModel
from dynamic_preferences.api.viewsets import GlobalPreferencesViewSet
from rest_framework.views import APIView

from .serializers import (
    QuestPolymorphicSerializer,
    QuestlineSerializer,
    PreferencesSerializer,
    BroadcastMessageSerializer,
    TeamSerializer,
    QuestListSerializer,
    QuestSyncSerializer,
)
from .models import Quest, Questline, BroadcastMessage, Team
from .permissions import QuestPermission, TeamPermission

from reportlab.pdfgen import canvas
import io


class ViewSetPermissionsMixin:
    """
    ViewSet with permissions
    - https://stackoverflow.com/a/61675657
    """

    def get_permissions(self):
        """
        Return the permission classes based on action.

        Look for permission classes in a dict mapping action to
        permission classes array, ie.:

        class MyViewSet(ViewSetActionPermissionMixin, ViewSet):
            ...
            permission_classes = [AllowAny]
            permission_action_classes = {
                'list': [IsAuthenticated]
                'create': [IsAdminUser]
                'my_action': [MyCustomPermission]
            }

            @action(...)
            def my_action:
                ...

        If there is no action in the dict mapping, then the default
        permission_classes is returned. If a custom action has its
        permission_classes defined in the action decorator, then that
        supercedes the value defined in the dict mapping.
        """
        try:
            return [
                permission()
                for permission in self.permission_action_classes[self.action]
            ]
        except KeyError:
            if self.action:
                action_func = getattr(self, self.action, {})
                action_func_kwargs = getattr(action_func, "kwargs", {})
                permission_classes = action_func_kwargs.get(
                    "permission_classes"
                )
            else:
                permission_classes = None

            return [
                permission()
                for permission in (
                    permission_classes or self.permission_classes
                )
            ]


class QuestViewSet(ViewSetPermissionsMixin, viewsets.ModelViewSet):
    """
    Viewset for Quest.
    """

    permission_action_classes = {
        "retrieve": [QuestPermission],
    }

    serializer_class = QuestPolymorphicSerializer
    queryset = Quest.objects.all()

    @action(detail=True)
    def qr(self, request, *args, **kwargs):
        quest = self.get_object()
        response = HttpResponse(content_type="image/png")
        root = request.build_absolute_uri('/')
        img = quest.generate_qr(root)
        img.save(response)
        return response

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class QuestlineViewSet(ViewSetPermissionsMixin, viewsets.ModelViewSet):
    """
    Viewset for Questline.
    """

    permission_action_classes = {
        "retrieve": [AllowAny],
        "list": [AllowAny]
    }

    serializer_class = QuestlineSerializer
    queryset = Questline.objects.all()

    @action(detail=True)
    def qr(self, request, *args, **kwargs):
        """Get all QR code with pdf."""

        questline = self.get_object()

        root = request.build_absolute_uri('/')

        buffer = io.BytesIO()
        p = canvas.Canvas(buffer)

        for quest in questline.quests.all():
            img = f"/tmp/opentour_{questline.id}_{quest.id}.png"
            quest.generate_qr(root).save(img)
            p.drawString(100, 800, "QR code opentour")
            p.drawString(100, 760, f"quest    :         {quest}")
            p.drawString(100, 740, f"quest id :         {quest.id}")

            try:
                if quest.password:
                    p.drawString(100, 720, ("password :         "
                                            f"{quest.password}"))
                if quest.embed_password:
                    p.drawString(100, 700, ("password is embedded"))
            except AttributeError:
                pass

            p.drawImage(img, 0, 0, 600, 600)
            p.showPage()

        p.save()
        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename="qr.pdf")


class PreferencesViewSet(ViewSetPermissionsMixin, GlobalPreferencesViewSet):

    permission_action_classes = {
        "retrieve": [AllowAny],
        "list": [AllowAny]
    }

    queryset = GlobalPreferenceModel.objects.all()
    serializer_class = PreferencesSerializer
    pagination_class = None

    def list(self, request, *args, **kwars):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        data = {obj["name"]: obj["raw_value"] for obj in serializer.data}
        return Response(data)


class BroadcastMessageViewSet(ViewSetPermissionsMixin, viewsets.ModelViewSet):
    """
    Viewset for BroadcastMessage.
    """

    permission_action_classes = {
        "retrieve": [AllowAny],
        "list": [AllowAny]
    }

    queryset = BroadcastMessage.objects.all()
    serializer_class = BroadcastMessageSerializer
    pagination_class = None


class TeamViewSet(ViewSetPermissionsMixin, viewsets.ModelViewSet):
    """
    Viewset for Team.
    """

    permission_action_classes = {
        "retrieve": [AllowAny],
        "list": [AllowAny],
        "create": [IsAuthenticated],
        "join": [IsAuthenticated],
        "leaderboard": [AllowAny],
        "qr": [TeamPermission]
    }

    serializer_class = TeamSerializer
    queryset = Team.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = TeamSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if request.user.team is not None \
           and len(request.user.team.users.all()) == 1:
            request.user.team.delete()

        instance = serializer.save()
        request.user.team = instance
        request.user.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)

    @action(detail=False)
    def join(self, request, *args, **kwargs):
        """Join a team."""
        try:
            team = Team.objects.get(
                invite_code=request.query_params.get("code")
            )
        except Team.DoesNotExist:
            team = None

        if team is None:
            return Response(data="Team not found", status=404)
        request.user.team = team
        request.user.save()

        return Response("Successfully joined team")

    @action(detail=False)
    def leaderboard(self, request, *args, **kwargs):
        """
        Same as list, but ordered by experience.
        """
        teams = sorted(Team.objects.all(), key=lambda team: -team.experience)

        page = self.paginate_queryset(teams)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(teams, many=True)
        return Response(serializer.data)

    @action(detail=True)
    def qr(self, request, *args, **kwargs):
        """Get all QR code with pdf."""

        team = self.get_object()
        root = request.build_absolute_uri('/')

        res = HttpResponse(content_type="image/png")
        team.generate_qr(root).save(res, 'png')
        return res


class QuestSyncView(APIView):
    """
    View that allow users to sync their finished quests.
    """
    permission_classes = [IsAuthenticated]

    def patch(self, request):
        serializer = QuestListSerializer(request.user, data=request.data)
        serializer.is_valid(raise_exception=True)

        for quest in serializer.validated_data['finished_quests']:
            request.user.finished_quests.add(quest)
        request.user.save()

        sync_resp = QuestSyncSerializer(request.user)
        return Response(sync_resp.data)
