import qrcode
from PIL import Image


def qr_from_url(url):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=10,
        border=4,
    )
    qr.add_data(url.url)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    logo = Image.open("assets/logo.png")
    logo = logo.convert("RGBA")
    box = (180, 180, 260, 260)
    logo = logo.resize((box[2] - box[0], box[3] - box[1]))
    img.paste(logo, box)
    return img
